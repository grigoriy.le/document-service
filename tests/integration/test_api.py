import os

import aiohttp
from aiohttp import test_utils

from prozorro_sale.document_service import sign
from prozorro_sale.document_service.api import create_app


class BaseAPITest(test_utils.AioHTTPTestCase):
    async def get_application(self):
        os.environ['BUCKET_NAME'] = ''
        os.environ['STORAGE_NAME'] = 'memory'
        return create_app()


class APITest(BaseAPITest):
    @property
    def multipart_obj(self):
        with aiohttp.MultipartWriter('form-data') as mpwriter:
            mpwriter.append(
                obj=b'bar',
                headers={
                    'Content-Type': 'plain/text',
                    'Content-Disposition': 'form-data; name="file"; filename="foo.txt"'
                }
            )
        return mpwriter

    @test_utils.unittest_run_loop
    async def test_ping(self):
        response = await self.client.get('/api/ping')
        data = await response.json()
        self.assertEqual(response.status, 200)
        self.assertEqual(data['text'], 'pong')

    @test_utils.unittest_run_loop
    async def test_upload_document_public(self):
        response = await self.client.put('/api/documents/public', data=self.multipart_obj)
        data = await response.read()
        self.assertEqual(response.status, 403)
        response = await self.client.put('/api/documents/public', data=self.multipart_obj,
                                         headers={"Authorization": "auction_token"})
        data = await response.read()
        self.assertEqual(response.status, 201)
        self.assertEqual(response.content_type, 'application/jwt')
        self.assertEqual(type(data), bytes)

    @test_utils.unittest_run_loop
    async def test_upload_document_private(self):
        response = await self.client.put('/api/documents/private', data=self.multipart_obj,
                                         headers={"Authorization": "auction_token"})
        data = await response.read()
        self.assertEqual(response.status, 201)
        self.assertEqual(response.content_type, 'application/jwt')
        self.assertEqual(type(data), bytes)

    @test_utils.unittest_run_loop
    async def test_get_document_head(self):
        response = await self.client.put('/api/documents/public', data=self.multipart_obj,
                                         headers={"Authorization": "auction_token"})
        data = await response.read()
        token = sign._decode_token(data)
        doc_id = token['id']
        response = await self.client.head(f'/api/documents/public/{doc_id}')
        self.assertEqual(response.status, 200)
        self.assertEqual(response.headers['X-Scope'], 'public')
        self.assertIn('X-Document-Type', response.headers)
        self.assertIn('X-SHA', response.headers)
        self.assertIn('X-Date-Created', response.headers)
        self.assertIn('ETag', response.headers)

    @test_utils.unittest_run_loop
    async def test_get_document_public(self):
        response = await self.client.put('/api/documents/public', data=self.multipart_obj,
                                         headers={"Authorization": "auction_token"})
        data = await response.read()
        token = sign._decode_token(data)
        doc_id = token['id']
        response = await self.client.get(f'/api/documents/public/{doc_id}')
        data = await response.read()
        self.assertEqual(response.status, 200)
        self.assertEqual(response.content_type, 'plain/text')
        self.assertEqual(data, b'bar')

    @test_utils.unittest_run_loop
    async def test_get_document_public_not_found(self):
        response = await self.client.get(f'/api/documents/public/123')
        self.assertEqual(response.status, 404)

    @test_utils.unittest_run_loop
    async def test_get_document_private(self):
        response = await self.client.put('/api/documents/private', data=self.multipart_obj,
                                         headers={"Authorization": "auction_token"})
        data = await response.read()
        token = data.decode()
        token_dict = sign._decode_token(data)
        doc_id = token_dict['id']
        response = await self.client.get(f'/api/documents/private/{doc_id}?token={token}')
        data = await response.read()
        self.assertEqual(response.status, 200)
        self.assertEqual(response.content_type, 'plain/text')
        self.assertEqual(data, b'bar')

    @test_utils.unittest_run_loop
    async def test_get_document_private_invalid_token(self):
        response = await self.client.get('/api/documents/private/123?token=123')
        data = await response.json()
        self.assertEqual(response.status, 403)
        self.assertEqual(data['error'], 'Invalid token. Not enough segments')

    @test_utils.unittest_run_loop
    async def test_get_document_private_no_token(self):
        response = await self.client.get('/api/documents/private/123')
        data = await response.json()
        self.assertEqual(response.status, 403)
        self.assertEqual(data['error'], 'No token provided')

    @test_utils.unittest_run_loop
    async def test_get_document_private_invalid_doc_id(self):
        response = await self.client.put('/api/documents/private', data=self.multipart_obj,
                                         headers={"Authorization": "auction_token"})
        data = await response.read()
        token = data.decode()
        response = await self.client.get(f'/api/documents/private/123?token={token}')
        data = await response.json()
        self.assertEqual(response.status, 403)
        self.assertEqual(data['error'], 'Invalid token. Invalid doc_id')

    @test_utils.unittest_run_loop
    async def test_verify_token_valid(self):
        token = sign.create_token({'some_data': 'data'})
        response = await self.client.get(f'/api/documents/verify_token?token={token}')
        self.assertEqual(response.status, 200)

    @test_utils.unittest_run_loop
    async def test_verify_token_no_token(self):
        response = await self.client.get(f'/api/documents/verify_token')
        data = await response.json()
        self.assertEqual(response.status, 403)
        self.assertEqual(data['error'], 'No token provided')

    @test_utils.unittest_run_loop
    async def test_verify_token_invalid_token(self):
        response = await self.client.get(f'/api/documents/verify_token?token=123')
        data = await response.json()
        self.assertEqual(response.status, 403)
        self.assertEqual(data['error'], 'Unverified. Not enough segments')

    @test_utils.unittest_run_loop
    async def test_get_public_key(self):
        response = await self.client.get(f'/api/documents/public_key')
        data = await response.read()
        self.assertEqual(response.status, 200)
        self.assertEqual(data.decode('utf-8'), sign.get_public_key())
