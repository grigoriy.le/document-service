from unittest.mock import MagicMock
from uuid import uuid4
from aiounittest import AsyncTestCase

from prozorro_sale.document_service.errors import KeyNotFound
from prozorro_sale.document_service.storages.memory_storage import MemoryStorage


async def get_text():
    return 'test'


async def get_file_data():
    return b'test'


class StorageTest(AsyncTestCase):

    def setUp(self) -> None:
        self.storage_obj = MemoryStorage('')
        self.doc_type = 'illustration'

    def get_mocked_data(self):
        main_mock = MagicMock()
        mock_doc_type = MagicMock()
        mock_file = MagicMock()

        mock_doc_type.name = 'documentType'
        mock_doc_type.text = get_text

        mock_file.name = 'file'
        mock_file.headers = {
            'Content-Disposition': 'form-data; name="file"; filename="coverage-report-25e0d270.zip"',
            'Content-Type': 'text/plain',
        }
        mock_file.read = get_file_data
        main_mock.__aiter__.return_value = [mock_doc_type, mock_file]
        return main_mock

    async def test_get_undefined_key(self):
        with self.assertRaises(KeyNotFound):
            await self.storage_obj.get(uuid=uuid4().hex, scope='private')

    async def test_upload(self):
        post_file = self.get_mocked_data()
        data = await self.storage_obj.upload(post_file, 'private', self.doc_type)
        self.assertIn('id', data)
        self.assertIn('scope', data)
        self.assertIn('documentType', data)
        self.assertIn('format', data)
        self.assertIn('sha', data)
        self.assertIn('hash', data)
        self.assertIn('dateCreated', data)

    async def test_get_file(self):
        post_file = self.get_mocked_data()
        upload_data = await self.storage_obj.upload(post_file, 'private', self.doc_type)

        response = await self.storage_obj.get(upload_data['id'], 'private')

        self.assertEqual(response.body, b'test')

    async def test_get_file_metadata(self):
        post_file = self.get_mocked_data()
        upload_data = await self.storage_obj.upload(post_file, 'private', self.doc_type)
        data = await self.storage_obj.get_metadata(upload_data['id'], 'private')
        self.assertIn('X-Scope', data.headers)
        self.assertIn('Content-Type', data.headers)
        self.assertIn('X-Document-Type', data.headers)
        self.assertIn('X-SHA', data.headers)
        self.assertIn('X-Date-Created', data.headers)
        self.assertIn('ETag', data.headers)
