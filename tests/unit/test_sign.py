import unittest

import jwt

from prozorro_sale.document_service import sign
from prozorro_sale.document_service import errors


class SignTest(unittest.TestCase):

    def test_get_public_key(self):
        public_key = sign.get_public_key()
        self.assertTrue(public_key)
        self.assertEqual(public_key, sign._PUBLIC_KEY)

    def test_get_private_key(self):
        private_key = sign.get_private_key()
        self.assertTrue(private_key)
        self.assertEqual(private_key, sign._PRIVATE_KEY)

    def test_encode_token(self):
        data = {'some_data': 'data'}
        token = sign._encode_token(data)
        self.assertEqual(type(token), str)

    def test_decode_token(self):
        data = {'some_data': 'data'}
        token = sign._encode_token(data)
        token_data = sign._decode_token(token)
        self.assertEqual(data, token_data)

    def test_create_token(self):
        data = {'some_data': 'data'}
        token = sign.create_token(data)
        token_data = sign._decode_token(token)
        self.assertTrue(isinstance(token_data['iat'], int))
        data['iat'] = int(data['iat'].timestamp())
        self.assertEqual(data, token_data)

    def test_validate_token(self):
        doc_id = '123'
        data = {'some_data': 'data', 'id': doc_id}
        token = sign.create_token(data)
        self.assertIsNone(sign.validate_token(token, doc_id))

    def test_validate_token_invalid_id(self):
        doc_id = '123'
        data = {'some_data': 'data', 'id': doc_id}
        token = sign.create_token(data)
        with self.assertRaises(errors.InvalidTokenError):
            sign.validate_token(token, '321')

    def test_validate_token_invalid_token(self):
        with self.assertRaises(errors.InvalidTokenError):
            sign.validate_token('123', '123')

    def test_verify_token(self):
        data = {'some_data': 'data'}
        token = sign.create_token(data)
        self.assertIsNone(sign.verify_token(token))

    def test_verify_token_invalid_token(self):
        with self.assertRaises(errors.UnverifiedTokenError):
            sign.verify_token('123')

    def test_verify_token_invalid_alg(self):
        data = {'some_data': 'data'}
        token = jwt.encode(
            data,
            sign.get_private_key(),
            algorithm='HS256',
            headers={'kid': 'ds'}
        )
        with self.assertRaises(errors.UnverifiedTokenError):
            sign.verify_token(token)
