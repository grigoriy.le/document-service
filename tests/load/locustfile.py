from locust import HttpLocust, TaskSet, task, between
import jwt
import requests

_PUBLIC_KEY = None
_ALGORITHM = 'RS256'
DOC_ID = None
FILE_CONTENT = None


def get_public_key():
    global _PUBLIC_KEY
    if not _PUBLIC_KEY:
        with open('secrets/ds-key.pub') as key_file:
            _PUBLIC_KEY = key_file.read()
    return _PUBLIC_KEY


def _decode_token(token):
    return jwt.decode(token, get_public_key(), algorithms=[_ALGORITHM])


class BaseDSFlow(TaskSet):
    """
    TaskSet to test base DS api working flow
    """
    filename = "3_mb.jpg"

    def __init__(self, parent):
        super(BaseDSFlow, self).__init__(parent)
        if not DOC_ID:
            self._load_file()

    def _load_file(self):
        global DOC_ID
        global FILE_CONTENT
        path_to_file = f"fixtures/{self.filename}"
        with open(path_to_file, 'rb') as f:
            FILE_CONTENT = f.read()

        response = requests.put(
            f"{self.locust.host}/api/documents/public",
            data={"documentType": "auctionProtocol"},
            files={"file": (self.filename, FILE_CONTENT, 'image/jpeg')}
        )
        token = _decode_token(response.content)
        DOC_ID = token['id']

    @task
    def upload_doc(self):
        self.client.put(
            "/api/documents/public",
            data={"documentType": "auctionProtocol"},
            files={"file": (self.filename, FILE_CONTENT, 'image/jpeg')},
        )

    @task
    def get_doc(self):
        self.client.get(
            f"/api/documents/public/{DOC_ID}"
        )


class BaseDSFlowTest(HttpLocust):
    task_set = BaseDSFlow
    wait_time = between(1, 2)
