# Locust load tests
## Installation
```
pip install locustio
```
## Running tests
### Starting web monitor
Run locally swift storage container like this https://hub.docker.com/r/morrisjobke/docker-swift-onlyone, 
create link between DS container and swift.
 
Select all tests from file, where HOST is host your document service(ex. `--host="http://0.0.0.0"` by default)
```
locust -f test/load/locustfile.py --host=HOST
```
Select specific tests
```
locust -f tests/load/locustfile.py SomeTaskClass --host=HOST 
```
### Open web-ui (default *:8089) and start new Locust swarm
