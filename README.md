# Repository for Prozorro.Sale Document-Service module

## This repo is intended to provide:

* document uploading (memory storage, s3, swift)

## Development guide

### System requirements

* make
* docker

### Running locally

```
make run
```

For setup local env, create py.env file:
```
{
        echo 'SERVICE_PORT=8080'
        echo 'STORAGE_NAME=memory'
        echo 'PYTHONASYNCIODEBUG=1'
        echo 'SWAGGER_DOC=1'
} > py.env
```


### Running tests

For unit tests:

```
make test-unit
```

For integration tests:

```
make test-integration
```

### For more information
<a href="./docs/endpoints_description.md"> Usage guide</a>
