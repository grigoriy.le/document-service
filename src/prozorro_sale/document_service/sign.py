from datetime import datetime

import jwt
from prozorro_sale.document_service.errors import InvalidTokenError, UnverifiedTokenError

_PRIVATE_KEY = None
_PUBLIC_KEY = None
_API_PUBLIC_KEY = None
_ALGORITHM = 'RS256'

PRIVATE_ACCESS = 'private'
PUBLIC_ACCESS = 'public'


def get_public_key():
    global _PUBLIC_KEY
    if not _PUBLIC_KEY:
        with open('/secrets/ds-key.pub') as key_file:
            _PUBLIC_KEY = key_file.read()
    return _PUBLIC_KEY


def get_api_public_key():
    global _API_PUBLIC_KEY
    if not _API_PUBLIC_KEY:
        with open('/secrets/api-key.pub') as key_file:
            _API_PUBLIC_KEY = key_file.read()
    return _API_PUBLIC_KEY


def get_private_key():
    global _PRIVATE_KEY
    if not _PRIVATE_KEY:
        with open('/secrets/ds-key') as key_file:
            _PRIVATE_KEY = key_file.read()
    return _PRIVATE_KEY


REGISTERED_KEYS = {
    'ds': get_public_key,
    'api': get_api_public_key
}


def _encode_token(document_data):
    return jwt.encode(
        document_data,
        get_private_key(),
        algorithm=_ALGORITHM,
        headers={'kid': 'ds'}
    )


def _decode_token(token):
    global REGISTERED_KEYS
    key_id = jwt.get_unverified_header(token)['kid']
    key = REGISTERED_KEYS[key_id]
    return jwt.decode(token, key(), algorithms=[_ALGORITHM])


def create_token(document_data):
    document_data['iat'] = datetime.utcnow()
    return _encode_token(document_data)


def validate_token(token, doc_id):
    try:
        encoded_jwt = _decode_token(token)
    except Exception as ex:
        raise InvalidTokenError(ex)

    if not encoded_jwt.get('id') == doc_id:
        # todo add expiration date of token
        raise InvalidTokenError('Invalid doc_id')


def verify_token(token):
    try:
        _decode_token(token)
    except (jwt.DecodeError, jwt.exceptions.InvalidKeyError,
            jwt.exceptions.InvalidAlgorithmError) as ex:
        raise UnverifiedTokenError(ex)
