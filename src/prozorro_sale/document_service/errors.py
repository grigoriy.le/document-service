class DocumentServiceAclException(Exception):
    pass


class InvalidTokenError(DocumentServiceAclException):
    pass


class UnverifiedTokenError(DocumentServiceAclException):
    pass


class RequiredTokenError(DocumentServiceAclException):
    pass


class DocumentServiceApplicationException(Exception):
    pass


class RetryRequestException(DocumentServiceApplicationException):
    pass


class KeyNotFound(KeyError):
    pass


class FileNotFound(KeyError):
    pass


class HeaderNotExists(KeyError):
    pass
