import asyncio
import cgi
import datetime
import hashlib
import logging
import os
from uuid import uuid4

import aiohttp
import prometheus_client
from aiohttp import web

from prozorro_sale.document_service.errors import RetryRequestException, FileNotFound, KeyNotFound, HeaderNotExists
from prozorro_sale.document_service.storages.base_storage import BaseStorage, DATETIME_FORMAT
from prozorro_sale.document_service.utils import critical

ERRORS = {
    404: KeyNotFound,
    408: TimeoutError,
    422: ValueError,
}

# 2 ** 16 - default linux socket buffer size
BUFF_SIZE = 2 ** 16 * 20
LOG = logging.getLogger('swift-storage')

get_document_session_latency = prometheus_client.Summary(
    'swift_session_get_document_latency', 'Get document request time')
put_document_session_latency = prometheus_client.Summary(
    'swift_session_put_document_latency', 'Put document request time')


def client():
    return aiohttp.ClientSession(
        connector=aiohttp.TCPConnector(
            limit=1,
            force_close=True,
            enable_cleanup_closed=True
        ),
        timeout=aiohttp.ClientTimeout(
            total=None,
            connect=None,
            sock_connect=2 * 60,
            sock_read=10 * 60
        ),
        read_bufsize=BUFF_SIZE
    )


class Auth:

    def __init__(self):
        auth_url = os.environ['SWIFT_AUTH_URL']
        self._auth_url = f'{auth_url}/v3/auth/tokens'
        self._project_id = os.environ['SWIFT_PROJECT_ID']
        self._user_id = os.environ['SWIFT_USER_ID']
        self._password = os.environ['SWIFT_PASSWORD']
        self._token = None
        self._expires_at = None
        asyncio.create_task(self.refresh_token())

    def get_token(self):
        return self._token

    @critical
    async def refresh_token(self):
        if self._expires_at:
            sleep_until = self._expires_at - datetime.datetime.now() - datetime.timedelta(seconds=120)
            await asyncio.sleep(sleep_until.seconds)
        auth_data = {
            "auth": {
                "identity": {
                    "methods": [
                        "password"
                    ],
                    "password": {
                        "user": {
                            "id": self._user_id,
                            "password": self._password
                        }
                    }
                },
                "scope": {
                    "project": {
                        "id": self._project_id
                    }
                }
            }
        }

        async with client() as session:
            resp = await session.post(self._auth_url, json=auth_data, ssl=False)
            if resp.status != 201:
                raise RuntimeError('Object store auth failed.. start to panic')
            data = await resp.json()
            self._token = resp.headers['X-Subject-Token']
            self._expires_at = datetime.datetime.strptime(data['token']['expires_at'], '%Y-%m-%dT%H:%M:%S.%fZ')
            asyncio.create_task(self.refresh_token())


class SwiftStorage(BaseStorage):
    storage_name = 'swift'

    def __init__(self, bucket):
        self.auth = Auth()
        self.url = f"{os.environ['SWIFT_HOST']}/v1/AUTH_{os.environ['SWIFT_PROJECT_ID']}"
        self._container = os.environ['SWIFT_CONTAINER']
        super().__init__(bucket)

    async def refresh_token(self):
        self.auth._expires_at = None
        await self.auth.refresh_token()
        raise RetryRequestException('Please try again later')

    async def upload(self, post_file, scope, doc_type):
        uuid = uuid4().hex
        sha_hash = hashlib.sha256()
        md5_hash = hashlib.md5()
        data = {'scope': scope, 'documentType': doc_type}
        object_url = f"{self.url}/{self._container}/{scope}_{uuid}"
        file_name = None
        date_created = None
        headers = {}
        async for field in post_file:
            if field.name in data:
                data[field.name] = await field.text()
            if field.name == 'file':
                file_name = cgi.parse_header(field.headers['Content-Disposition'])[1]['filename']

                async def read_data(_field) -> bytes:
                    while file_data := await _field.read_chunk(BUFF_SIZE):
                        sha_hash.update(file_data)
                        md5_hash.update(file_data)
                        yield file_data

                headers = {
                    'X-Auth-Token': self.auth.get_token()
                }
                with put_document_session_latency.time():
                    async with client() as session:
                        response = await session.put(object_url, ssl=False, data=read_data(field), headers=headers)

                        if response.status == 401:
                            await self.refresh_token()
                        elif response.status > 201:
                            if response.status == 404:
                                # todo: update with another error codes
                                censored_url = object_url.replace(os.environ['SWIFT_PROJECT_ID'], "<swift_pr_id>")
                                LOG.error(f"Got 404 from swift storage during upload document. Url {censored_url}")
                            raise ERRORS.get(response.status, Exception)
                        date_created = datetime.datetime.now().strftime(DATETIME_FORMAT)
                        try:
                            headers.update({
                                'Content-Type': field.headers['Content-Type'],
                                'Content-Disposition': field.headers['Content-Disposition'],
                                'X-Object-Meta-scope': scope,
                                'X-Object-Meta-documentType': data['documentType'],
                                'X-Object-Meta-sha': sha_hash.hexdigest(),
                                'X-Object-Meta-hash': 'md5:' + md5_hash.hexdigest(),
                            })
                        except KeyError as ex:
                            raise HeaderNotExists(ex)

        if not file_name:
            raise FileNotFound

        async with client() as session:
            response = await session.post(object_url, ssl=False, headers=headers)
            if response.status == 401:
                await self.refresh_token()
            elif response.status > 202:
                raise ERRORS.get(response.status, Exception)

        return {
            'id': uuid,
            'scope': headers['X-Object-Meta-scope'],
            'filename': file_name,
            'documentType': headers['X-Object-Meta-documentType'],
            'format': headers['Content-Type'],
            'sha': headers['X-Object-Meta-sha'],
            'hash': headers['X-Object-Meta-hash'],
            'dateCreated': date_created
        }

    async def get(self, uuid, scope, request):
        headers = {'X-Auth-Token': self.auth.get_token()}
        object_url = f"{self.url}/{self._container}/{scope}_{uuid}"
        with get_document_session_latency.time():
            async with client() as session:
                response = await session.get(object_url, ssl=False, headers=headers)
                if response.status == 401:
                    await self.refresh_token()
                elif response.status > 200:
                    raise ERRORS[response.status]
                headers = {
                    'Content-Type': response.headers['Content-Type'],
                    'Content-Disposition': response.headers['Content-Disposition'],
                }
                stream = web.StreamResponse(headers=headers)
                await stream.prepare(request)
                while chunk := await response.content.read(BUFF_SIZE):
                    await stream.write(chunk)
                await stream.write_eof()

                return stream

    async def get_metadata(self, uuid, scope):
        headers = {'X-Auth-Token': self.auth.get_token()}
        object_url = f"{self.url}/{self._container}/{scope}_{uuid}"
        async with client() as session:
            response = await session.head(object_url, ssl=False, headers=headers)
            if response.status == 401:
                await self.refresh_token()
            elif response.status > 200:
                raise ERRORS[response.status]

            metadata = {
                'X-Scope': scope,
                'X-Document-Type': response.headers['X-Object-Meta-documentType'],
                'Content-Type': response.headers['Content-Type'],
                'X-SHA': response.headers['X-Object-Meta-sha'],
                'X-Date-Created': response.headers['Last-Modified'],
                'ETag': response.headers['X-Object-Meta-hash']
            }
            return web.Response(headers=metadata, status=200)
