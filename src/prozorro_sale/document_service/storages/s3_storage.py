from datetime import datetime
from uuid import uuid4
import hashlib
import cgi
import aiobotocore
import os
from aiohttp import web

from botocore.exceptions import ClientError

from prozorro_sale.document_service.storages.base_storage import BaseStorage, DATETIME_FORMAT
from prozorro_sale.document_service.errors import FileNotFound, KeyNotFound, HeaderNotExists

CLIENT = None


def get_client():
    global CLIENT
    if not CLIENT:
        CLIENT = aiobotocore.get_session().create_client(
            's3',
            region_name=os.environ['BUCKET_HOST'],
            aws_secret_access_key=os.environ['BUCKET_SECRET_KEY'],
            aws_access_key_id=os.environ['BUCKET_ACCESS_KEY']
        )
    return CLIENT


class S3Storage(BaseStorage):

    storage_name = 's3'

    async def upload(self, post_file, scope, doc_type):
        uuid = uuid4().hex
        data = {'scope': scope, 'documentType': doc_type}
        file_name = None
        async for field in post_file:
            if field.name in data:
                data[field.name] = await field.text()
            if field.name == 'file':
                try:
                    file_name = cgi.parse_header(field.headers['Content-Disposition'])[1]['filename']
                    file_data = await field.read()
                    data.update({
                        'Content-Type': field.headers['Content-Type'],
                        'Content-Disposition': field.headers['Content-Disposition'],
                        'body': file_data,
                        'sha': hashlib.sha256(file_data).hexdigest(),
                        'hash': 'md5:' + hashlib.md5(file_data).hexdigest(),
                        'dateCreated': datetime.now().strftime(DATETIME_FORMAT)
                    })
                except KeyError as ex:
                    raise HeaderNotExists(ex)

        if 'body' not in data:
            raise FileNotFound

        content_type = data['Content-Type']
        client = get_client()
        await client.put_object(
            Bucket=self.bucket,
            Key=f"{scope}/{uuid}",
            Body=data.pop('body'),
            ContentDisposition=data.pop('Content-Disposition'),
            ContentType=data.pop('Content-Type'),
            Metadata=data
        )
        data['id'] = uuid
        return {
            'id': uuid,
            'scope': data['scope'],
            'filename': file_name,
            'documentType': data['documentType'],
            'format': content_type,
            'sha': data['sha'],
            'hash': data['hash'],
            'dateCreated': data['dateCreated']
        }

    async def get(self, uuid, scope, request):
        client = get_client()
        try:
            response = await client.get_object(Bucket=self.bucket, Key=f"{scope}/{uuid}")
        except ClientError:
            raise KeyNotFound(uuid)
        content = await response['Body'].read()
        response_headers = response['ResponseMetadata']['HTTPHeaders']

        return web.Response(
            body=content,
            headers={
                'Content-Disposition': response_headers['content-disposition'],
                'Content-Type': response_headers['content-type']
            },
            status=200
        )

    async def get_metadata(self, uuid, scope):
        client = get_client()
        try:
            response = await client.head_object(Bucket=self.bucket, Key=f"{scope}/{uuid}")
        except ClientError:
            raise KeyNotFound(uuid)

        metadata_response = response['Metadata']
        metadata = {
            'X-Scope': metadata_response['scope'],
            'X-Document-Type': metadata_response['documenttype'],
            'Content-Type': metadata_response['ContentType'],
            'X-SHA': metadata_response['sha'],
            'X-Date-Created': metadata_response['datecreated'],
            'ETag': metadata_response['hash']
        }
        return web.Response(headers=metadata, status=200)
