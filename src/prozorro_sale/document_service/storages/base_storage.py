import os

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S.%f'
BUCKET_NAME = os.environ.get('BUCKET_NAME', '')


class BaseStorage:
    storage_name = None

    def __init__(self, bucket):
        self.bucket = bucket

    async def upload(self, post_file, scope, doc_type):
        raise NotImplementedError("Upload method must be implemented in subclass")

    async def get(self, uuid, scope, request):
        raise NotImplementedError("Get method must be implemented in subclass")

    async def get_metadata(self, uuid, scope):
        raise NotImplementedError("Get_metadata method must be implemented in subclass")


async def create_bucket_instance(app):
    storage_name = os.environ.get('STORAGE_NAME', 'memory')
    storage_list = [klass for klass in BaseStorage.__subclasses__() if klass.storage_name == storage_name]
    if not storage_list:
        raise AttributeError(f"No such storage {storage_name}")
    storage = storage_list.pop()
    app.storage = storage(BUCKET_NAME)
