import hashlib
import cgi
from datetime import datetime
from uuid import uuid4
from aiohttp import web

from prozorro_sale.document_service.storages.base_storage import BaseStorage, DATETIME_FORMAT
from prozorro_sale.document_service.errors import FileNotFound, KeyNotFound, HeaderNotExists


class MemoryStorage(BaseStorage):
    storage_name = 'memory'

    def __init__(self, bucket):
        super().__init__(bucket)
        self.storage = {}

    async def upload(self, post_file, scope, doc_type):
        uuid = uuid4().hex
        data = {'scope': scope, 'documentType': doc_type}
        file_name = None
        async for field in post_file:
            if field.name in data:
                data[field.name] = await field.text()
            if field.name == 'file':
                try:
                    file_name = cgi.parse_header(field.headers['Content-Disposition'])[1]['filename']
                    file_data = await field.read()
                    data.update({
                        'Content-Type': field.headers['Content-Type'],
                        'Content-Disposition': field.headers['Content-Disposition'],
                        'body': file_data,
                        'sha': hashlib.sha256(file_data).hexdigest(),
                        'hash': 'md5:' + hashlib.md5(file_data).hexdigest(),
                        'dateCreated': datetime.now().strftime(DATETIME_FORMAT)
                    })
                except KeyError as ex:
                    raise HeaderNotExists(ex)
        if 'body' not in data:
            raise FileNotFound

        self.storage[uuid] = data
        return {
            'id': uuid,
            'scope': data['scope'],
            'filename': file_name,
            'documentType': data['documentType'],
            'format': data['Content-Type'],
            'sha': data['sha'],
            'hash': data['hash'],
            'dateCreated': data['dateCreated']
        }

    async def get(self, uuid, scope, request=None):
        try:
            data = self.storage[uuid]
        except KeyError:
            raise KeyNotFound(uuid)
        return web.Response(
            body=data['body'],
            headers={
                'Content-Disposition': data['Content-Disposition'],
                'Content-Type': data['Content-Type']
            },
            status=200
        )

    async def get_metadata(self, uuid, scope):
        try:
            data = self.storage[uuid]
        except KeyError:
            raise KeyNotFound(uuid)

        metadata = {
            'X-Scope': data['scope'],
            'Content-Type': data['Content-Type'],
            'X-Document-Type': data['documentType'],
            'X-SHA': data['sha'],
            'X-Date-Created': data['dateCreated'],
            'ETag': data['hash']
        }
        return web.Response(headers=metadata, status=200)
