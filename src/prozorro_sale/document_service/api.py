import uvloop
import asyncio
import os

from aiohttp import web
from aiohttp.web_exceptions import Response
from aiohttp_swagger import setup_swagger, swagger_path

import prozorro_sale
from aiotask_context import task_factory
from prozorro_sale import auth, tools, metrics
from prozorro_sale.document_service.storages import *  # noqa
from prozorro_sale.document_service import sign
from prozorro_sale.document_service.storages import base_storage
from prozorro_sale.document_service.utils import get_token, excepts_errors_middleware


LOGGER = tools.logging.get_custom_logger(__name__)

SWAGGER_DOC_AVAILABLE = os.getenv('SWAGGER_DOC', False)


@swagger_path('/swagger/ping.yaml')
async def ping(request):
    return web.json_response({'text': 'pong'})


async def api_version(request):
    return web.json_response({'api_version': prozorro_sale.version})


@swagger_path('/swagger/verify_token.yaml')
async def verify_token(request):
    sign.verify_token(get_token(request))
    return Response(status=200)


@swagger_path('/swagger/upload_document.yaml')
@auth.check_access
async def upload_document(request):
    if request.content_type != 'multipart/form-data':
        return web.json_response(data={'error': "Content-type 'multipart/form-data' is required"}, status=415)
    try:
        data = await request.multipart()
    except ValueError:
        return web.json_response(data={
            'error': "Broken content-type 'multipart/form-data'."
            "Check you don't override content-type header value set automatically."
        }, status=415)
    scope = request.match_info['scope']
    doc_type = request.query.get('documentType', '')
    document_data = await request.app.storage.upload(data, scope, doc_type)
    LOGGER.info(f'Uploaded document id {document_data["id"]}')
    encoded_jwt = sign.create_token(document_data)
    return Response(body=encoded_jwt, status=201, content_type='application/jwt')


@swagger_path('/swagger/get_document.yaml')
async def get_document(request):
    doc_id = request.match_info['doc_id']
    scope = request.match_info['scope']
    if scope == sign.PRIVATE_ACCESS:
        sign.validate_token(get_token(request), doc_id)
    return await request.app.storage.get(doc_id, scope, request)


@swagger_path('/swagger/get_document_meta.yaml')
async def get_document_meta(request):
    doc_id = request.match_info['doc_id']
    scope = request.match_info['scope']
    return await request.app.storage.get_metadata(doc_id, scope)


@swagger_path('/swagger/get_public_key.yaml')
async def get_public_key(request):
    return Response(body=sign.get_public_key(), status=200)


async def on_shutdown(app):
    LOGGER.info('Shutting down application')


def setup_routes(app):
    app.router.add_get('/api', api_version, allow_head=False)
    app.router.add_get('/api/ping', ping, allow_head=False)
    app.router.add_put('/api/documents/{scope:(public|private)}', upload_document)
    app.router.add_get('/api/documents/{scope:(public|private)}/{doc_id}', get_document, allow_head=False)
    app.router.add_head('/api/documents/{scope:(public|private)}/{doc_id}', get_document_meta)
    app.router.add_get('/api/documents/verify_token', verify_token, allow_head=False)
    app.router.add_get('/api/documents/public_key', get_public_key, allow_head=False)


def create_app():
    loop = asyncio.get_event_loop()
    loop.set_task_factory(task_factory)
    tools.logging.configure_logging()
    auth.load_auth('/secrets/auth.yml')
    app = web.Application(middlewares=[
        excepts_errors_middleware,
        auth.context_middleware,
        tools.logging.request_id_middleware
    ], loop=loop)
    setup_routes(app)
    app.on_startup.append(base_storage.create_bucket_instance)
    app.on_shutdown.append(on_shutdown)
    if SWAGGER_DOC_AVAILABLE:
        setup_swagger(
            app,
            ui_version=3,
            api_version=prozorro_sale.version,
            security_definitions={'Bearer': {'type': 'apiKey', 'name': 'Authorization', 'in': 'header'}},
        )
    return app


if __name__ == '__main__':
    uvloop.install()
    app = create_app()
    app_wrapper = metrics.ApplicationWrapper()
    service_port = os.environ.get('SERVICE_PORT', 80)
    app_wrapper.add_web_app(
        app,
        service_port,
        access_log_class=tools.logging.CustomAccessLogger,
        tcp_params={
            'backlog': 256,
            'reuse_address': True,
            'reuse_port': True
        }
    )
    app_wrapper.run_all()
