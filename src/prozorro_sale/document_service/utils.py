import sys
import asyncio

from aiohttp import web
from aiohttp.client_exceptions import ClientConnectorError, ServerTimeoutError, ClientPayloadError
from aiohttp.web_response import Response

from prozorro_sale.document_service import errors
from prozorro_sale import tools

LOG = tools.logging.get_custom_logger(__name__)


def get_token(request):
    token = request.query.get('token')
    if not token:
        raise errors.RequiredTokenError
    return token


@web.middleware
async def excepts_errors_middleware(request, handler):
    try:
        return await handler(request)
    except errors.UnverifiedTokenError as ex:
        return web.json_response(data={'error': f'Unverified. {ex}'}, status=403)
    except errors.InvalidTokenError as ex:
        return web.json_response(data={'error': f'Invalid token. {ex}'}, status=403)
    except errors.FileNotFound:
        return web.json_response(data={'error': "file key is required in form-data"}, status=400)
    except errors.HeaderNotExists as ex:
        return web.json_response(data={'error': f'No required header. {ex}'}, status=400)
    except errors.RetryRequestException:
        return web.json_response(data={'error': "Try again"}, status=503)
    except errors.RequiredTokenError:
        return web.json_response(data={'error': 'No token provided'}, status=403)
    except errors.KeyNotFound:
        return Response(status=404)
    except asyncio.TimeoutError:
        LOG.exception('Got client session timeout exception')
        return web.json_response(
            data={'error': 'Catch up TimeoutError on storage requests. Please try again.'},
            status=503
        )
    except (ClientConnectorError, ServerTimeoutError, ClientPayloadError) as ex:
        LOG.warning(f'Storage connect error - {ex}', stack_info=False)
        return web.json_response(
            data={'error': 'Catch up TimeoutError on storage requests. Please try again.'},
            status=503
        )


def critical(func):
    async def wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except Exception as error:
            LOG.exception(f'Got an error while swift token refreshing: {error}')
            sys.exit(1)
    return wrapper
