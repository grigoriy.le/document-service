import asyncio
import os
import prozorro_sale
import uvloop
from aiohttp import web
from aiohttp.web_exceptions import Response
from aiotask_context import task_factory
from prozorro_sale import tools
from prozorro_sale.document_service import sign
from prozorro_sale.document_service.storages import *  # noqa
from prozorro_sale.document_service.storages import base_storage
from prozorro_sale.document_service.utils import excepts_errors_middleware

LOG = tools.logging.get_custom_logger(__name__)


async def version(request):
    return web.json_response({'api_version': prozorro_sale.version})


async def ping(request):
    return web.json_response({'text': 'pong'})


async def upload_document(request):
    if request.content_type != 'multipart/form-data':
        return web.json_response(data={'error': "Content-type 'multipart/form-data' is required"}, status=415)
    try:
        data = await request.multipart()
    except ValueError:
        return web.json_response(data={
            'error': "Broken content-type 'multipart/form-data'."
                     "Check you don't override content-type header value set automatically."
        }, status=415)
    scope = request.match_info['scope']
    doc_type = request.query.get('documentType', '')
    document_data = await request.app.storage.upload(data, scope, doc_type)
    LOG.info(f'Uploaded document id {document_data["id"]}')
    encoded_jwt = sign.create_token(document_data)
    return Response(body=encoded_jwt, status=201, content_type='application/jwt')


async def on_startup(app):
    LOG.info('Application is starting...')


async def on_shutdown(app):
    LOG.info('Shutting down application')


def setup_routes(app):
    app.router.add_get('/api', version)
    app.router.add_get('/api/ping', ping)
    app.router.add_put('/api/documents/{scope:(public|private)}', upload_document)


def create_app():
    loop = asyncio.get_event_loop()
    loop.set_task_factory(task_factory)
    tools.logging.configure_logging()
    app = web.Application(middlewares=[
        excepts_errors_middleware,
        tools.logging.request_id_middleware
    ], loop=loop)
    setup_routes(app)
    app.on_startup.append(base_storage.create_bucket_instance)
    app.on_shutdown.append(on_shutdown)
    return app


if __name__ == '__main__':
    uvloop.install()
    app = create_app()
    service_port = os.environ.get('SERVICE_PORT', 80)
    web.run_app(
        app,
        port=service_port,
        access_log_class=tools.logging.CustomAccessLogger
    )
