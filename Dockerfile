FROM python:3-slim as base

WORKDIR /document_service
ENV PYTHONUNBUFFERED True
COPY requirements.txt .
RUN pip install -i https://pypi-int.prozorro.sale/ -r requirements.txt
COPY secrets /secrets
COPY swagger /swagger
COPY config /config

FROM base as test_base
COPY test-requirements.txt .
RUN pip install -r test-requirements.txt

FROM base as prod
COPY src/ .
ARG version=unknown
RUN echo $version && sed -i "s/##VERSION##/$version/g" prozorro_sale/__init__.py

FROM test_base as test
COPY src/ .
COPY tests ./tests
ARG version=unknown
RUN echo $version && sed -i "s/##VERSION##/$version-dev/g" prozorro_sale/__init__.py

FROM prod
