# Document Service endpoints usage

## 1. Upload document

### Endpoint url - api/documents/[private, public]?documentType=[auctionProtocol, cancellation, etc]

### Response example
#### Response for api/documents/private?documentType=auctionProtocol with file name ds.txt will be jwt token, 
#### for example 
```json
eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImRzIn0.eyJpZCI6IjdiNGY1ZjUwMzBkMjRkODg5OWYzZTQ2MGI4YjgyMzNjIiwic2NvcGUiOiJw
cml2YXRlIiwiZmlsZW5hbWUiOiJkcy50eHQiLCJkb2N1bWVudFR5cGUiOiJyZWplY3Rpb25Qcm90b2NvbCIsImZvcm1hdCI6InRleHQvcGxhaW4iLCJzaGEi
OiIwMzM2NzhlN2YwNDYyODFkNGJjZDViNzgwN2NlNTE5ZTFmZTBlNjBkYWExMWRmMjY0Y2Y0OTViMTJlZTFlOWZmIiwiaGFzaCI6Im1kNTo0MjQ5NGFkMDFh
ZmQwNWVlMGRjNjljZDM2ZDZmYmMwNiIsImRhdGVDcmVhdGVkIjoiMjAyMC0wNi0xNyAxNzozNzo1NS43NjM1MTUiLCJpYXQiOjE1OTI0MTU0NzV9.JETL4I6
oFNx3so2kzrE-BG8nEjFqsjfRkCs3JPLlNSgHgL35pl03uh9ghl6f-ItkfQBL8iXrlvWi8Tth_PU_dsrAMKoOvjp9ixIGRno0L6sIJCmpi0xhlM1Seqy21C4
HflL6OignJxs1J6xKkxsTltZqdjINXhSFqmsqEyt8J14
```

#### and decoded jwt 
```json
{
  "id": "7b4f5f5030d24d8899f3e460b8b8233c",
  "scope": "private",
  "filename": "ds.txt",
  "documentType": "auctionProtocol",
  "format": "text/plain",
  "sha": "033678e7f046281d4bcd5b7807ce519e1fe0e60daa11df264cf495b12ee1e9ff",
  "hash": "md5:42494ad01afd05ee0dc69cd36d6fbc06",
  "dateCreated": "2020-06-17 17:37:55.763515",
  "iat": 1592415475
}
```
#### Content of ds.txt - "Test data for DS docs"

### Decryption resource rink  - https://jwt.io/
### Decryption algorithm  - RS256

## 2. Get document

### Endpoint url - api/documents/[private, public]/:token_id?token=token
##### where token_id = 7b4f5f5030d24d8899f3e460b8b8233c (from example above),
##### and token is token you've got after document placement

### Response body - "Test data for DS docs"

## 3. Get public key
#### This is optional and not required to work with DS
#### Endpoint url - api/documents/public_key

#### Response example (type: text/plain)
```json
-----BEGIN PUBLIC KEY-----
PUBLIC KEY
-----END PUBLIC KEY-----
```

## 4. Verify token
#### This is optional and not required to work with DS
#### Endpoint url - /api/documents/verify_token?token
##### where token is token you've got after document placement

#### Response example
##### If token is OK you'll get status 200, otherwise error message
```json
{
    "error": "Unverified. Signature verification failed"
}
```
##### or if token format is not valid

```json
{
    "error": "Unverified. Invalid crypto padding"
}
```

## 5. Get document meta
#### This is optional and not required to work with DS
#### Endpoint url - api/documents/[private, public]/:token_id
##### where token_id = 177eb5d424564e03afcb07ce1a298c9b (from example above)
