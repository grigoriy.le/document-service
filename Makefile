PROZORRO_DS_IMAGE ?= prozorro-document-service:develop
PROZORRO_DS_IMAGE_TEST ?= prozorro-document-service:develop-test
CI_COMMIT_SHORT_SHA ?= $(shell git rev-parse --short HEAD)
GIT_STAMP ?= $(shell git describe)
CHART_MUSEUM_URL ?= "https://helm.prozorro.sale/api/charts"

ifdef CI
  REBUILD_IMAGES_FOR_TESTS =
else
  REBUILD_IMAGES_FOR_TESTS = docker-build
endif

push-helm-package:
	curl \
		--user $(CHART_MUSEUM_USER):$(CHART_MUSEUM_PASS) \
		--data-binary "@document-service-$(GIT_STAMP).tgz" \
		$(CHART_MUSEUM_URL)

helm-lint:
	helm lint helm/document-service

helm-build:
	helm package helm/document-service --app-version=$(GIT_STAMP) --version=$(GIT_STAMP)

docker-build:
	docker build --build-arg version=$(GIT_STAMP) -t $(PROZORRO_DS_IMAGE) .
	docker build --target=test --build-arg version=$(GIT_STAMP) -t $(PROZORRO_DS_IMAGE_TEST) .

run: $(REBUILD_IMAGES_FOR_TESTS)
	docker run -e "SWAGGER_DOC=1" -p 80:80 $(PROZORRO_DS_IMAGE) python -m prozorro_sale.document_service.api

test-unit: $(REBUILD_IMAGES_FOR_TESTS)
	docker rm -f $(CI_COMMIT_SHORT_SHA) || true
	docker run \
		--name $(CI_COMMIT_SHORT_SHA) \
		$(PROZORRO_DS_IMAGE_TEST) \
		nosetests --with-coverage --cover-package=prozorro_sale tests.unit -v
	docker cp $(CI_COMMIT_SHORT_SHA):/document_service/.coverage .coverage.unit

test-integration: $(REBUILD_IMAGES_FOR_TESTS)
	docker rm -f integration-$(CI_COMMIT_SHORT_SHA) || true
	docker run \
		--name integration-$(CI_COMMIT_SHORT_SHA) \
		$(PROZORRO_DS_IMAGE_TEST) \
		nosetests --with-coverage --cover-package=prozorro_sale tests.integration -v
	docker cp $(CI_COMMIT_SHORT_SHA):/document_service/.coverage .coverage.integration

publish-coverage:
	docker rm -f $(CI_COMMIT_SHORT_SHA) || true
	docker run -d --name $(CI_COMMIT_SHORT_SHA) $(PROZORRO_DS_IMAGE_TEST) sleep infinity
	docker cp .coverage.unit $(CI_COMMIT_SHORT_SHA):/tmp/
	docker cp .coverage.integration $(CI_COMMIT_SHORT_SHA):/tmp/
	docker exec $(CI_COMMIT_SHORT_SHA) bash -c "cd /tmp && coverage combine && coverage report && coverage html -d cover-html"
	docker cp $(CI_COMMIT_SHORT_SHA):/tmp/cover-html cover-html
	docker rm -f $(CI_COMMIT_SHORT_SHA)

version:
	$(eval GIT_TAG ?= $(shell git describe --abbrev=0))
	$(eval VERSION ?= $(shell read -p "Version: " VERSION; echo $$VERSION))
	echo "Tagged release $(VERSION)\n" > Changelog-$(VERSION).txt
	git log --oneline --no-decorate --no-merges $(GIT_TAG)..HEAD >> Changelog-$(VERSION).txt
	git tag -a -e -F Changelog-$(VERSION).txt $(VERSION)
